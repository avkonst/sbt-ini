
import sbt.project

lazy val sbtini = (project in file("."))
    .settings(
        name := "sbt-ini",
        version := "1.0.0",
        // this is scala version required for sbt 0.13
        scalaVersion := "2.10.6",
        organization := "me.avkonst",
        // it is an extension of sbt tool
        sbtPlugin := true
    )

licenses += ("MIT", url("http://opensource.org/licenses/MIT"))

bintrayPackageLabels := Seq("sbt", "plugin", "shared", "settings", "ini")

bintrayReleaseOnPublish in ThisBuild := false

bintrayRepository := "maven"
