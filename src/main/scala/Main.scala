package me.avkonst.sbtini

import java.io.File
import scala.collection.mutable.ListBuffer
import scala.io.Source

object ^ {
    val settingFileName = "build.ini"

    def apply(key: String) : String = {
        if (settings.isEmpty) {
            var curDir = Option(new File(".").getAbsoluteFile())
            val tried = ListBuffer[String]()
            while (curDir.isDefined) {
                val f = new File(curDir.get, settingFileName)
                tried.append(f.getAbsolutePath())
                if (f.exists()) {
                    settingsLocation = f.getAbsolutePath()
                    // parse init file in plain scala
                    val content = Source.fromFile(f).getLines()
                    var curSection = ""
                    settings = Some(content.map(i => i.trim())
                        .filter(i => !i.startsWith("#") || !i.startsWith(";") || i.isEmpty)
                        .map(i => i.split("=", 2).map(j => j.trim()))
                        .map(i => if (i.length == 1) {
                                curSection = i(0).drop(1).dropRight(1).trim()
                                None
                            } else {
                                Some((s"$curSection.${i(0)}" -> i(1)))
                            })
                        .filter(i => i.isDefined).map(i => i.get).toMap)
                    curDir = None
                } else {
                    // jump to higher level directory
                    curDir = Option(curDir.get.getParentFile())
                }
            }
            if (settings.isEmpty) {
                val triedPaths = tried.mkString("\n")
                throw new Exception(s"Failure to locate solution settings file, tried:\n${triedPaths}")
            }
        }
        settings.get.getOrElse[String](key,
            throw new Exception(s"Failure to locate '$key' setting in the file ${settingsLocation}"))
    }

    private var settings: Option[Map[String, String]] = None
    private var settingsLocation: String = ""
}
