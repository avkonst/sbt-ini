# SBT INI Plugin
SBT plugin to enable sharing of common settings
for multi-component projects with separate build.sbt files.

## Usage
- enable the plugin in `project/plugins.sbt` file:
```
    resolvers += Resolver.url(
        "bintray-avkonst-maven",
        url("http://dl.bintray.com/avkonst/maven"))(
        Resolver.ivyStylePatterns)

    addSbtPlugin("me.avkonst" % "sbt-ini" % "1.0.0")
```
- create `build.ini` file:
  - side by side with `built.sbt` file
  - OR in any of the parent folders of `built.sbt`.
- add settings to the `build.ini` file you would like to share
with `build.sbt` file(s). Use any names of sections and properties,
for example:
```
    [scala]
    version = 2.11.7

    [solution]
    organization = org.example
    version = 1.0.0-SNAPSHOT

    [packages]
    org.scalatest = 2.2.4
```
- import settings in `build.sbt` file(s) and use it, for example:
```
    import me.avkonst.sbtini.^

    organization := ^("solution.organization"),
    scalaVersion := ^("scala.version"),
    version := ^("solution.version")

    libraryDependencies ++= Seq(
        "org.scalatest" %% "scalatest" % ^("packages.org.scalatest") % "test"
    )
```

Note: `^` symbol indicates that a value is sourced
from external `build.ini` file by the provided section-property name.
If you prefer more explicit and verbose name instead of `^` symbol,
you may define it, for example named as `ini`:
```
    import me.avkonst.sbtini.{^ => ini}

    organization := ini("solution.organization"),
    scalaVersion := ini("scala.version"),
    version := ini("solution.version")

    libraryDependencies ++= Seq(
        "org.scalatest" %% "scalatest" % ini("packages.org.scalatest") % "test"
    )
```

